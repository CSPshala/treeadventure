#ifndef BINARYTREETEST
#define BINARYTREETEST

#include <ITest.h>
#include <iostream>

class BinaryTreeTest : public ITest
{
public:
	BinaryTreeTest() {}
	~BinaryTreeTest() override;

private:
	bool _runTests() const override;
	const char* _testName() const override;
};

#endif