#include <iostream>
#include <vector>

#if defined OS_DARWIN
#elif defined OS_WINDOWS
#include <windows.h>
#endif

#include <ITest.h>

// Test Includes
#include "BinaryTreeTest.h"

void doTests();

std::vector<ITest*> createTests();
void runTests(std::vector<ITest*>& tests);
void cleanUpTests(std::vector<ITest*>& tests);

int main(int argV, char** argC)
{
	std::cout << "*** Tree Test Runner START ****" << std::endl;

	doTests();

	std::cout << "*** Tree Test Runner END ****" << std::endl;

	std::cin.get();

	return 0;
}

void doTests()
{
	std::vector<ITest*> tests = createTests();
	runTests(tests);
	cleanUpTests(tests);
}

std::vector<ITest*> createTests()
{
	std::vector<ITest*> tests;

	tests.push_back(new BinaryTreeTest());

	return tests;
}

void runTests(std::vector<ITest*>& tests)
{
	for(ITest* test : tests)
		test->runTests();
}

void cleanUpTests(std::vector<ITest*>& tests)
{
	for(ITest* test : tests)
		delete test;

	tests.clear();
}