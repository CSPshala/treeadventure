#ifndef ITEST
#define ITEST
// Interface for test classes that sub projects will implement

class ITest
{
public:
	ITest() {}
	virtual ~ITest() {}

	bool runTests() const;
	const char* getTestName() {return _testName();}
protected:
	virtual bool _runTests() const = 0;
	virtual const char* _testName() const = 0;
private:
};
#endif