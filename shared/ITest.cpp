#include "ITest.h"

#include <iostream>

bool ITest::runTests() const
{
	std::cout << "->Running Test: " << _testName() << "<-" << std::endl;
	bool result = _runTests();
	std::cout << "<-Test Done: " << _testName() << "->" << std::endl;
	return result;
}